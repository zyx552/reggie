package com.itheima.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.common.BaseContext;
import com.itheima.common.R;
import com.itheima.entity.User;
import com.itheima.service.UserService;
import com.itheima.utils.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("/user")
@SuppressWarnings("all")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 生成验证码
     * @param user
     * @param session
     * @return
     */
    @PostMapping("/sendMsg")
    public R<String> sendMsg(@RequestBody User user, HttpSession session) {
        //获取页面传来的手机号
        String phone = user.getPhone();
        //判断是否为空
        if (StringUtils.isNotEmpty(phone)) {
            //通过工具类生成4位验证码
            String code = ValidateCodeUtils.generateValidateCode(4).toString();
//            //存入session域，key是页面传来的手机号，value是验证码
//            session.setAttribute(phone, code);

            //使用redis缓存存储，有效时间60秒
            redisTemplate.opsForValue().set(phone,code,60L, TimeUnit.SECONDS);
            log.info("手机验证码为{}", code);
            return R.success("手机验证码发送成功");
        }
        return R.error("手机验证码发送失败");
    }

    /**
     * 用户登录，验证码校验
     * @param map
     * @param session
     * @return
     */
    @PostMapping("/login")
    public R<User> login(@RequestBody Map map, HttpSession session) {
        //获取用户传来的手机号和验证码
        String phone = map.get("phone").toString();
        String code = map.get("code").toString();

//        //获取session域中的验证码
//        String codeInSession = (String) session.getAttribute(phone);
        String codeInSession = (String) redisTemplate.opsForValue().get(phone);
        //判断验证码是否相同
        if (codeInSession != null && codeInSession.equals(code)){
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            //判断是否为新用户
            queryWrapper.eq(User::getPhone,phone);
            User user = userService.getOne(queryWrapper);
            //是新用户则将信息添加到user表
            if (user == null){
                user = new User();
                user.setPhone(phone);
                user.setStatus(1);
                userService.save(user);
            }
            //将用户登录session域
            session.setAttribute("user",user.getId());
            return R.success(user);
        }
        return R.error("登录失败");
    }

    /**
     * 退出登录
     * @param session
     * @return
     */
    @PostMapping("loginout")
    public R<String> logout(HttpSession session){
        //获取当前用户登录的id
        Long currentId = BaseContext.getCurrentId();
        //在session域中删除
        session.removeAttribute(currentId.toString());
        return R.success("退出成功");
    }
}
