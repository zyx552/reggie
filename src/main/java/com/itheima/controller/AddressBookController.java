package com.itheima.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.itheima.common.R;
import com.itheima.entity.AddressBook;
import com.itheima.service.AddressBookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("addressBook")
@Api(tags = "收获地址")
@SuppressWarnings("all")
public class AddressBookController {

    @Autowired
    private AddressBookService addressBookService;

    /**
     * 添加地址
     * @param addressBook
     * @return
     */
    @ApiOperation(value = "添加收货地址")
    @PostMapping
    public R<String> add(@RequestBody AddressBook addressBook,HttpSession session){
        log.info("新增地址为{}",addressBook);
        addressBook.setUserId((Long) session.getAttribute("user"));
        addressBookService.save(addressBook);
        return R.success("添加地址成功");
    }

    /**
     * 查询地址列表
     * @return
     */
    @GetMapping("list")
    public R<List<AddressBook>> getAll(HttpSession session){
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId,(Long) session.getAttribute("user"));
        List<AddressBook> list = addressBookService.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 修改地址默认值
     * @param addressBook
     * @return
     */
    @PutMapping("default")
    public R<String> updateDefault(@RequestBody AddressBook addressBook, HttpSession session){
        LambdaUpdateWrapper<AddressBook> updateWrapper1 = new LambdaUpdateWrapper<>();
        updateWrapper1.eq(AddressBook::getUserId,(Long) session.getAttribute("user"));
        updateWrapper1.set(AddressBook::getIsDefault,0);
        addressBookService.update(updateWrapper1);

        LambdaUpdateWrapper<AddressBook> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(AddressBook::getId,addressBook.getId());
        updateWrapper.set(AddressBook::getIsDefault,1);
        addressBookService.update(updateWrapper);
        return R.success("修改成功");
    }

    /**
     * 修改地址回显
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public R<AddressBook> getOne(@PathVariable Long id){
        log.info("修改的地址id为{}",id);
        AddressBook addressBook = addressBookService.getById(id);
        return R.success(addressBook);
    }

    /**
     * 修改地址
     * @param addressBook
     * @return
     */
    @PutMapping
    public R<String> update(@RequestBody AddressBook addressBook){
        addressBookService.updateById(addressBook);
        return R.success("修改成功");
    }

    /**
     * 删除地址
     * @param ids
     * @return
     */
    @DeleteMapping
    public R<String> delete(Long ids){
        addressBookService.removeById(ids);
        return R.success("删除地址成功");
    }

    /**
     * 设置默认值
     * @return
     */
    @GetMapping("default")
    public R<AddressBook> getDefault(HttpSession session){
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getIsDefault, 1);
        queryWrapper.eq(AddressBook::getUserId,(Long) session.getAttribute("user"));
        AddressBook addressBook = addressBookService.getOne(queryWrapper);
        return R.success(addressBook);
    }
}
