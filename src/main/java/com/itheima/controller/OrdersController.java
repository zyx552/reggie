package com.itheima.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.common.R;
import com.itheima.entity.Orders;
import com.itheima.service.OrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;


@Slf4j
@RestController
@RequestMapping("order")
@SuppressWarnings("all")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    /**
     * 下单
     * @param orders
     * @return
     */
    @PostMapping("submit")
    public R<String> submit(@RequestBody Orders orders,HttpSession session) {
        ordersService.submit(orders,session);
        return R.success("添加成功");
    }

    /**
     * 订单分页查询
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("userPage")
    public R<Page> getAll(Integer page, Integer pageSize, HttpSession session){
        Long currentId =(Long) session.getAttribute("user");
        //分页查询
        Page<Orders> page1 = new Page<>(page,pageSize);
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        //按时间排序
        queryWrapper.orderByDesc(Orders::getOrderTime);
        queryWrapper.eq(Orders::getUserId,currentId);
        ordersService.page(page1,queryWrapper);

        return R.success(page1);
    }

    /**
     * 服务端订单查询
     * @param page
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    @GetMapping("page")
    public R<Page> getOrders(Integer page, Integer pageSize, String number, String beginTime, String endTime){
        Page orders = ordersService.getOrders(page, pageSize, number, beginTime, endTime);
        return R.success(orders);
    }

    /**
     * 派送
     * @param orders
     * @return
     */
    @PutMapping
    public R<String> updataStatus(@RequestBody Orders orders){
        ordersService.updateById(orders);
        return R.success("修改成功");
    }

    /**
     * 再来一单
     * 有Bug，点击后会跳转到主页面
     * @param id
     * @return
     */
    @PostMapping("again")
    public R<Orders> again(Integer id){
        Orders orders = ordersService.getById(id);
        return R.success(orders);
    }

}
