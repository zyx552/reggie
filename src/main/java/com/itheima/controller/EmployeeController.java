package com.itheima.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.common.R;
import com.itheima.entity.Employee;
import com.itheima.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/employee")
@SuppressWarnings("all")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /**
     * 员工登录
     *
     * @param request
     * @param employee
     * @return
     */
    @PostMapping("/login")
    public R<Employee> login(HttpServletRequest request, @RequestBody Employee employee) {
        log.info("员工尝试登录...username =" + employee.getUsername() + "   password =" + employee.getPassword());

        //获取页面输入的密码
        String password = employee.getPassword();
        //转换成MD5格式
        password = DigestUtils.md5DigestAsHex(password.getBytes());

        //通过用户名查询
        LambdaQueryWrapper<Employee> elq = new LambdaQueryWrapper<>();
        //查询数据库中是否含有用户输入的username
        elq.eq(Employee::getUsername, employee.getUsername());
        //调用方法查询
        Employee oneEmp = employeeService.getOne(elq);

        //判断用户名是否存在
        if (oneEmp == null) {
            return R.error("用户名不存在!");
        }
        //判断密码是否正确
        if (!oneEmp.getPassword().equals(password)) {
            return R.error("密码错误!");
        }
        //判断账号状态
        if (oneEmp.getStatus().equals(0)) {
            return R.error("账号已禁用！");
        }
        //将id存入session域，访问其他页面时不需要在从数据库中重新查询数据登录
        request.getSession().setAttribute("employee", oneEmp.getId());
        log.info("员工已登录，员工id为：{}", request.getSession().getAttribute("employee"));
        //查询成功返回
        return R.success(oneEmp);
    }


    /**
     * 退出登录
     *
     * @param request
     * @return
     */
    @PostMapping("/logout")
    public R<String> logout(HttpServletRequest request) {
        //将id在session域中删除
        request.getSession().removeAttribute("employee");
        //返回
        return R.success("退出成功");
    }


    /**
     * 添加员工
     *
     * @param request
     * @param employee
     * @return
     */
    @PostMapping
    public R<String> add(HttpServletRequest request, @RequestBody Employee employee) {
        log.info("添加员工 = " + employee.toString());
        //设置初始密码，并且通过MD5加密
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
//        //设置添加时间
//        employee.setCreateTime(LocalDateTime.now());
//        //设置修改时间
//        employee.setUpdateTime(LocalDateTime.now());
//        //设置操作添加的员工id
//        employee.setCreateUser((Long) (request.getSession().getAttribute("employee")));
//        //设置操作修改的员工id
//        employee.setUpdateUser((Long) (request.getSession().getAttribute("employee")));
        //添加
        employeeService.save(employee);
        log.info("添加用户成功 username = " + employee.getUsername());
        //返回成功添加的响应
        return R.success("添加成功!");
    }

    /**
     * 分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public R<Page> pageR(Integer page, Integer pageSize, String name) {
        log.info("分页查询  page = {},pageSize = {}, name = {}", page, pageSize, name);
        //创建分页构造器
        Page employeePage = new Page(page, pageSize);
        // 创建分页条件
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        //name不为空则进行查询
        queryWrapper.like(!StringUtils.isEmpty(name), Employee::getName, name);
        //按日期排序
        queryWrapper.orderByDesc(Employee::getUpdateTime);
        //查询
        employeeService.page(employeePage, queryWrapper);
        //查询成功给出响应
        return R.success(employeePage);
    }

    /**
     * 修改员工账号
     *
     * @param request
     * @param employee
     * @return
     */
    @PutMapping
    public R<String> upDate(HttpServletRequest request, @RequestBody Employee employee) {
        //查询当前登录员工的id
        Long employee1 = (Long) request.getSession().getAttribute("employee");
        //设置修改时间
//        employee.setUpdateTime(LocalDateTime.now());
        //设置操作修改的员工id
//        employee.setUpdateUser(employee1);
        //修改
        employeeService.updateById(employee);

        return R.success("员工信息修改成功");
    }

    /**
     * 编辑员工信息
     * 回显
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<Employee> upDateById(HttpServletRequest request, @PathVariable Long id) {
        log.info("修改员工信息 id = {}", id);
        log.info("线程id为"+Thread.currentThread().getId());
        //通过id查询
        Employee employee = employeeService.getById(id);
        //判断是否查询到
        if (employee != null) {
            return R.success(employee);
        }
        return R.error("没有查询到对应的员工信息");
    }
}
