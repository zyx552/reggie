package com.itheima.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.common.CustomException;
import com.itheima.common.R;
import com.itheima.dto.DishDto;
import com.itheima.entity.*;
import com.itheima.service.*;
import com.itheima.service.impl.DishServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/dish")
@SuppressWarnings("all")
public class DishController {

    @Autowired
    private DishServiceImpl dishService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private DishFlavorService dishFlavorService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private SetmealDishService setmealDishService;

    @Autowired
    private SetmealService setmealService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 新增菜品
     *
     * @param dishDto
     * @return
     */
    @PostMapping
    public R<String> save(@RequestBody DishDto dishDto) {
        log.info(dishDto.toString());
        dishService.saveWithFlavor(dishDto);
        //新增前删除缓存
        String key = "dish_" + dishDto.getCategoryId() + "_1";
        redisTemplate.delete(key);
        return R.success("添加成功");
    }


    /**
     * 菜品分页查询
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    public R<Page> getAll(Integer page, Integer pageSize, String name) {
        //创建分页构造器对象
        Page<Dish> dishPage = new Page<>(page, pageSize);
        Page<DishDto> dishDtoPage = new Page<>();

        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        //添加排序条件
        queryWrapper.orderByDesc(Dish::getCreateTime);
        //添加模糊查询
        queryWrapper.like(name != null, Dish::getName, name);
        //执行查询
        dishService.page(dishPage, queryWrapper);

        //------注意-----
        // 前端页面需要查询对应的菜品分类name,dish里面的数据关联了菜品分类的id属性，但是没有名称name属性

        //对象拷贝  dto里面增加了name属性
        BeanUtils.copyProperties(dishPage, dishDtoPage);
        //获取dishPage里面的数据
        List<Dish> records = dishPage.getRecords();
        //创建dto集合
        List<DishDto> list = new ArrayList<>();
        //通过循环，将dishPage里面dish对象的id查询出来，然后封装到dto对象里面
        for (int i = 0; i < records.size(); i++) {
            //创建dto对象，里面增加了菜品分类的name属性
            DishDto dishDto = new DishDto();
            //获取dish对象  注：dish里面只有id，没有name，需要通过id查询name
            Dish dish = records.get(i);
            //将dish的属性拷贝到dto，因为dto对象里面的属性没有值
            BeanUtils.copyProperties(dish, dishDto);
            //获取dish菜品分类的id
            Long id = dish.getCategoryId();
            //查询
            Category category = categoryService.getById(id);
            //非空判断
            if (category != null) {
                //获取菜品分类name
                String name1 = category.getName();
                //赋值
                dishDto.setCategoryName(name1);
            }
            //list是一个空集合，将封装好数据的dto对象添加到集合中
            list.add(dishDto);
        }
        //添加到分页对象里面
        dishDtoPage.setRecords(list);

        return R.success(dishDtoPage);
    }

    /**
     * 修改菜品数据回显
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<DishDto> get(@PathVariable Long id) {
        DishDto byIdWithFlavor = dishService.getByIdWithFlavor(id);
        return R.success(byIdWithFlavor);
    }

    /**
     * 修改菜品
     *
     * @param dishDto
     * @return
     */
    @PutMapping

    public R<String> update(@RequestBody DishDto dishDto) {
        dishService.updateWithFlavor(dishDto);

        /*为什么不删除单个分类？
          因为对于修改操作，用户是可以修改菜品的分类的，
          如果用户修改了菜品的分类，那么原来分类下将少一个菜品，新的分类下将多一个菜品，
          这样的话，两个分类下的菜品列表数据都发生了变化
         */

        //获取所有菜品的缓存数据
        Set keys = redisTemplate.keys("dish_*");
        //删除缓存
        redisTemplate.delete(keys);
        return R.success("修改成功");
    }

    /**
     * 新增套餐页面查询
     * 用户端查询
     *
     * @param dish
     * @return
     */
    @GetMapping("/list")
    public R<List<DishDto>> getListDish(Dish dish) {
        ArrayList<DishDto> dishDtos = null;
        //设置key
        String key = "dish_" + dish.getCategoryId() + "_" + dish.getStatus();
        //查询缓存中是否有数据
        dishDtos = (ArrayList<DishDto>) redisTemplate.opsForValue().get(key);
        //有数据则返回
        if (dishDtos != null) {
            return R.success(dishDtos);
        }

        //创建查询条件对象
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(dish != null, Dish::getCategoryId, dish.getCategoryId());
        //查询是否被禁用
        queryWrapper.eq(Dish::getStatus, 1);
        //按时间排序
        queryWrapper.orderByAsc(Dish::getCreateTime);
        List<Dish> list = dishService.list(queryWrapper);

        //查询菜品口味
        dishDtos = new ArrayList<>();
        for (Dish dish1 : list) {
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(dish1, dishDto);

//            Long categoryId = dish1.getCategoryId();
//
//            Category category = categoryService.getById(categoryId);
//
//            if (category != null){
//                dishDto.setCategoryName(category.getName());
//            }
            //根据菜品id查询口味
            Long id = dish1.getId();
            LambdaQueryWrapper<DishFlavor> queryWrapper1 = new LambdaQueryWrapper<>();
            queryWrapper1.eq(DishFlavor::getDishId, id);
            List<DishFlavor> list1 = dishFlavorService.list(queryWrapper1);
            dishDto.setFlavors(list1);
            dishDtos.add(dishDto);
        }
        //从数据库查询完数据储存到缓存中

//        redisTemplate.opsForValue().set(key,dishDtos);

        redisTemplate.opsForValue().set(key, dishDtos);

        return R.success(dishDtos);
    }

    /**
     * 删除菜品
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    public R<String> deleteDish(@RequestParam List<Long> ids) {
        List<Dish> dishes = dishService.listByIds(ids);
        //遍历查询集合中的数据又没有启售状态的
        for (Dish dish : dishes) {
            if (dish.getStatus() == 1) {
                throw new CustomException("该菜品为启售状态，删除失败！");
            }
        }
        dishService.removeByIds(ids);

        return R.success("删除成功");
    }

    /**
     * 修改状态，停售
     *
     * @param ids
     * @return
     */
    @PostMapping("status/0")
    public R<String> updataStatus0(@RequestParam List<Long> ids) {
        //通过id获取菜品
        List<Dish> dishes = dishService.listByIds(ids);
        //设置套餐条件
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        //设置购物车条件
        LambdaQueryWrapper<ShoppingCart> queryWrapper1 = new LambdaQueryWrapper<>();
        //循环遍历id
        for (Long id : ids) {
            //  SetmealDish setmealDish = setmealDishService.getById(id);
            //查询套餐中菜品id
            queryWrapper.eq(SetmealDish::getDishId, id);
            List<SetmealDish> setmealDishes = setmealDishService.list(queryWrapper);
            //遍历菜品id
            for (SetmealDish setmealDish : setmealDishes) {
                //通过套餐菜品id获取套餐
                Long setmealId = setmealDish.getSetmealId();
                Setmeal setmeal = setmealService.getById(setmealId);
                //查询套餐是否为售卖状态
                if (setmeal.getStatus() == 1) {
                    log.error("------------------chu");
                    throw new CustomException("该菜品所在的套餐正在售卖，停售失败！");
                }
            }
            //判断购物车中的菜品id
            queryWrapper1.eq(ShoppingCart::getDishId, id);
            ShoppingCart shoppingCart = shoppingCartService.getOne(queryWrapper1);
            //查到就说明用户已经加购
            if (shoppingCart != null){
                throw new CustomException("已经有用户将该菜品加入了购物车，停售失败");
            }
        }
        //以上情况都没有发生，执行停售修改
        for (Dish dish : dishes) {
            log.info("------------");
            dish.setStatus(0);
        }
        dishService.updateBatchById(dishes);
        //清除缓存
        Set dish_ = redisTemplate.keys("dish_*");
        redisTemplate.delete(dish_);
        return R.success("修改成功");
    }

    /**
     * 修改状态：启售
     *
     * @param ids
     * @return
     */
    @PostMapping("status/1")
    public R<String> updateStatus1(@RequestParam List<Long> ids) {
        //通过id查询菜品
        List<Dish> dishes = dishService.listByIds(ids);
        //循环遍历更改状态
        for (Dish dish : dishes) {
            dish.setStatus(1);
        }
        dishService.updateBatchById(dishes);
        //清除缓存
        Set dish_ = redisTemplate.keys("dish_*");
        redisTemplate.delete(dish_);
        return R.success("修改成功");
    }


}
