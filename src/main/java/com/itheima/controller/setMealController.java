package com.itheima.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.common.CustomException;
import com.itheima.common.R;
import com.itheima.dto.SetmealDto;
import com.itheima.entity.*;
import com.itheima.service.CategoryService;
import com.itheima.service.DishService;
import com.itheima.service.SetmealDishService;
import com.itheima.service.ShoppingCartService;
import com.itheima.service.impl.SetmealServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/setmeal")
@SuppressWarnings("all")
public class setMealController {

    @Autowired
    private SetmealServiceImpl setMealService;

    @Autowired
    private SetmealDishService setMealDishService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private DishService dishService;
    /**
     * 新建套餐
     *
     * @param setMealDto
     * @return
     */
    @PostMapping
    //清除setmealCache名称下,所有的缓存数据
    @CacheEvict(value = "setmealCache",allEntries = true)
    public R<String> save(@RequestBody SetmealDto setMealDto) {
        //为什么要使用dto对象接收前端传过来的数据？
        //因为setmeal中没有套餐对应的菜品字段，dto对象拓展了菜品字段
        setMealService.saveWithDish(setMealDto);
        return R.success("添加成功");
    }


    /**
     * 套餐分页查询
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    public R<Page> getAll(Integer page, Integer pageSize, String name) {
        log.error("执行-------------------");
        //创建分页对象
        Page<Setmeal> setMealPage = new Page<>(page, pageSize);
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        //设置分页条件  按时间降序排列
        queryWrapper.orderByDesc(Setmeal::getCreateTime);
        //模糊查询
        queryWrapper.like(name != null, Setmeal::getName, name);
        //注：现在查询出来的数据没有套餐分类的name，只有套餐id
        setMealService.page(setMealPage, queryWrapper);

        //创建page对象，泛型为dto类型，dto里面拓展了套餐分类的name
        Page<SetmealDto> setmealDtoPage = new Page<>();
        //拷贝  不需要拷贝records，因为它的泛型不是dto类型
        BeanUtils.copyProperties(setMealPage, setmealDtoPage, "records");
        //获取查询出来的数据
        List<Setmeal> records = setMealPage.getRecords();
        //创建dto对象集合
        ArrayList<SetmealDto> setmealDtos = new ArrayList<>();
        for (Setmeal record : records) {
            //创建dto对象
            SetmealDto setmealDto = new SetmealDto();
            //拷贝
            BeanUtils.copyProperties(record, setmealDto);
            //通过套餐id查询套餐name，然后封装到dto对象里面
            Category category = categoryService.getById(record.getCategoryId());
            //非空判断
            if (category != null) {
                //封装并添加到集合
                setmealDto.setCategoryName(category.getName());
                setmealDtos.add(setmealDto);
            }
        }
        setmealDtoPage.setRecords(setmealDtos);
        return R.success(setmealDtoPage);
    }

    /**
     * 删除套餐方法
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    //清除setmealCache名称下,所有的缓存数据
    @CacheEvict(value = "setmealCache",allEntries = true)
    public R<String> delete(@RequestParam List<Long> ids) {
        log.info("删除套餐，id为{}", ids);
        setMealService.deleteWithDish(ids);
        return R.success("删除套餐成功");
    }

    /**
     * 修改状态为停售
     *
     * @param ids
     * @return
     */
    @PostMapping("status/0")
    //清除setmealCache名称下,所有的缓存数据
    @CacheEvict(value = "setmealCache",allEntries = true)
    public R<String> updateStatus0(@RequestParam List<Long> ids) {
        log.info(ids.toString());
        //通过id查询菜品
        List<Setmeal> setmeals = setMealService.listByIds(ids);

        //判断是否有用户加购
            LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        for (Long id : ids) {
            queryWrapper.eq(ShoppingCart::getSetmealId, id);
            ShoppingCart shoppingCart = shoppingCartService.getOne(queryWrapper);
            if (shoppingCart != null){
                throw new CustomException("有用户将该套餐加入了购物车，修改失败");
            }
        }

        //将套餐的状态修改为0启售
        for (Setmeal setmeal : setmeals) {
            setmeal.setStatus(0);
        }
        //执行修改
        setMealService.updateBatchById(setmeals);
        return R.success("修改状态成功！");
    }

    /**
     * 修改状态为启售
     *
     * @param ids
     * @return
     */
    @PostMapping("/status/1")
    @CacheEvict(value = "setmealCache",allEntries = true)
    public R<String> updateStatus1(@RequestParam List<Long> ids) {
        //查询套餐中的菜品
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        for (Long id : ids) {
            queryWrapper.eq(SetmealDish::getSetmealId, id);
            List<SetmealDish> list = setMealDishService.list(queryWrapper);

            //根据套餐菜品表查询菜品
            for (SetmealDish setmealDish : list) {
                Dish dish = dishService.getById(setmealDish.getDishId());
                if (dish.getStatus() != 1){
                    throw new CustomException("该套餐中的有菜品已经被停售，修改失败！");
                }
            }
        }


        //通过id查询菜品
        List<Setmeal> setmeals = setMealService.listByIds(ids);
        for (Setmeal setmeal : setmeals) {
            //将套餐的状态修改为1启售
            setmeal.setStatus(1);
        }
        //执行修改
        setMealService.updateBatchById(setmeals);
        return R.success("修改状态成功！");
    }

    /**
     * 用户端获取菜品口味
     *
     * @param setmeal
     * @return
     */
    @GetMapping("list")

    @Cacheable(value = "setmealCache", key = "#setmeal.categoryId + '_' + #setmeal.status")
    public R<List<Setmeal>> list(Setmeal setmeal) {
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        //设置条件，菜品为启售
        queryWrapper.eq(setmeal.getCategoryId() != null, Setmeal::getCategoryId, setmeal.getCategoryId());
        queryWrapper.eq(setmeal.getStatus() != null, Setmeal::getStatus, setmeal.getStatus());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        List<Setmeal> list = setMealService.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 用户端套餐详情
     *
     * @param id
     * @return
     */
    @GetMapping("dish/{id}")
    public R<List<SetmealDish>> getOne(@PathVariable Long id) {
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        //根据用户传入的id查询套餐里面的菜品
        queryWrapper.eq(SetmealDish::getSetmealId, id);
        List<SetmealDish> list = setMealDishService.list(queryWrapper);

        return R.success(list);
    }

    /**
     * 修改套餐回显
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public R<SetmealDto> getSetMeal(@PathVariable Long id){
        SetmealDto setmealDto = setMealService.getWithDishDto(id);
        return R.success(setmealDto);
    }
}
