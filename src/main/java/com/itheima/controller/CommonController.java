package com.itheima.controller;


import com.itheima.common.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

/**
 * 文件上传下载
 */
@Slf4j
@RestController
@RequestMapping("common")
@SuppressWarnings("all")
public class CommonController {

    //文件存储路径
    @Value("${ipath.path}")
    private String basePath;

    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    @PostMapping("upload")
    public R<String> upload(MultipartFile file) {
        //file是一个临时文件，本次请求完成后临时文件就会销毁
        //获取用户上传的文件名
        String originalFilename = file.getOriginalFilename();
        log.info("接收到用户上传文件文件--> " + originalFilename);
        //切割获得文件名后缀
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        //通过UUID随机生成文件名
        String fileName = UUID.randomUUID().toString() + suffix;

        File dir = new File(basePath);
        //判断目录是否存在，没有则创建
        if (!dir.exists()) {
            dir.mkdirs();
        }

        try {
            //将临时文件存放到指定位置
            file.transferTo(new File(basePath + fileName));
        } catch (IOException e) {
            e.printStackTrace();
            log.error("文件上传失败，请检查..");
        }
        return R.success(fileName);
    }

    /**
     * 文件下载
     *
     * @param name     请求的文件名
     * @param response
     */
    @GetMapping("/download")
    public void download(String name, HttpServletResponse response) {
        log.info("请求路径为 "+name);
        try {
            //创建文件输入流，并传入用户网页请求的文件名
            FileInputStream fileInputStream = new FileInputStream(new File(basePath + name));
            //获取输出流对象，通过输出流将文件写回到浏览器
            ServletOutputStream outputStream = response.getOutputStream();
            //设置文件类型
            response.setContentType("image/jpeg");

            //使用工具类拷贝
//            IOUtils.copy(fileInputStream, outputStream);
//            fileInputStream.close();
//            outputStream.close();

            //原始方法
            int len = 0;
            byte[] bytes = new byte[1024];
            while ((len = fileInputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, len);
                outputStream.flush();
            }
            //关闭资源
            outputStream.close();
            fileInputStream.close();
        } catch (Exception e) {
            log.error("下载文件失败....请检查服务器...");
        }

    }

}
