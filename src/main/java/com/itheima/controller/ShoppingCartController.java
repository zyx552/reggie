package com.itheima.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.common.R;
import com.itheima.entity.ShoppingCart;
import com.itheima.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("shoppingCart")
@SuppressWarnings("all")
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;



    /**
     * 用户端查询购物车
     * @return
     */
    @GetMapping("list")
    public R<List<ShoppingCart>> list(HttpSession session) {
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        //根据用户登录id查询购物车

        queryWrapper.eq(ShoppingCart::getUserId, session.getAttribute("user"));

        queryWrapper.eq(ShoppingCart::getUserId, (Long) session.getAttribute("user"));

        List<ShoppingCart> list = shoppingCartService.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 加入购物车
     * @param shoppingCart
     * @return
     */
    @PostMapping("add")
    public R<String> add(@RequestBody ShoppingCart shoppingCart, HttpSession session){
        //获取用户当前登录id
        Long currentId = (Long) session.getAttribute("user");
        //设置用户id
        shoppingCart.setUserId(currentId);
        //查询加购的是菜品还是套餐
        Long dishId = shoppingCart.getDishId();
        //设置条件查询对象
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, currentId);

        //判断添加是菜品还是套餐
        if (dishId != null){
            //是菜品,然后查询购物车中是否已经加购
            queryWrapper.eq(ShoppingCart::getDishId,dishId);
        }else {
            //是套餐，然后查询购物车中是否已经加购
            queryWrapper.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        }
        //执行查询
        ShoppingCart cartServiceOne = shoppingCartService.getOne(queryWrapper);

        if (cartServiceOne != null){
            Integer number = cartServiceOne.getNumber();
            //已经加购，在原基础上+1
            cartServiceOne.setNumber(number+1);
            shoppingCartService.updateById(cartServiceOne);
        }else{
            //没有加购
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartService.save(shoppingCart);
        }


        return R.success("加入购物车成功");
    }

    /**
     * 从购物车-1
     * @param shoppingCart
     * @param session
     * @return
     */
    @PostMapping("sub")
    public R<String> sub(@RequestBody ShoppingCart shoppingCart, HttpSession session){
        //获取当前登录的用户id
        Long user = (Long)session.getAttribute("user");
        shoppingCart.setUserId(user);

        //假如当前操作的是菜品
        Long dishId = shoppingCart.getDishId();
        //设置条件
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,shoppingCart.getUserId());
        if (dishId != null){
            //如果是菜品
            queryWrapper.eq(ShoppingCart::getDishId, shoppingCart.getDishId());
        }else{
            //如果是套餐
            queryWrapper.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        }
        //获取购物车订单
        ShoppingCart shoppingCartServiceOne = shoppingCartService.getOne(queryWrapper);

        if (shoppingCartServiceOne.getNumber() == 1){
            //如果当前加购的数量为1，则直接从购物车中删除
            shoppingCartService.removeById(shoppingCartServiceOne.getId());
        }else{
            //如果不为1，则从基础上 - 1
            Integer number = shoppingCartServiceOne.getNumber();
            shoppingCartServiceOne.setNumber(number - 1);
            shoppingCartService.updateById(shoppingCartServiceOne);
        }
        return R.success("修改成功");
    }




    /**
     * 清空购物车
     * @return
     */
    @DeleteMapping("clean")
    public R<String> clean(HttpSession session){
        //获取当前用户登录id  根据id删除购物车
        Long currentId = (Long) session.getAttribute("user");
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, currentId);
        shoppingCartService.remove(queryWrapper);
        return R.success("清空购物车成功");
    }
}
