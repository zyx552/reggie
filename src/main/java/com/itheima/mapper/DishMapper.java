package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.Dish;


public interface DishMapper extends BaseMapper<Dish> {
}
