package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.OrderDetail;

public interface OrdersDetailMapper extends BaseMapper<OrderDetail> {
}
