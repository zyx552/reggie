package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.common.CustomException;
import com.itheima.dto.SetmealDto;
import com.itheima.entity.Dish;
import com.itheima.entity.Setmeal;
import com.itheima.entity.SetmealDish;
import com.itheima.mapper.SetmealMapper;
import com.itheima.service.DishService;
import com.itheima.service.SetmealDishService;
import com.itheima.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@SuppressWarnings("all")
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {

    @Autowired
    private SetmealDishService setmealDishService;

    @Autowired
    private DishService dishService;

    /**
     * 新建套餐
     * @param setmealDto
     */
    @Override
    @Transactional
    public void saveWithDish(SetmealDto setmealDto) {
        log.info(setmealDto.getSetmealDishes().toString());

        //添加图片
        List<SetmealDish> setmealDishes1 = setmealDto.getSetmealDishes();
        for (SetmealDish setmealDish : setmealDishes1) {
            Long dishId = setmealDish.getDishId();
            Dish dish = dishService.getById(dishId);
            String image = dish.getImage();
            setmealDish.setImage(image);
        }
        setmealDto.setSetmealDishes(setmealDishes1);

        //先添加套餐基本信息
        this.save(setmealDto);

        //获取套餐关联的菜品集合
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        //菜品集合中的数据没有关联套餐，为每一个数据添加套餐id
        for (SetmealDish setmealDish : setmealDishes) {
            setmealDish.setSetmealId(setmealDto.getId());
        }
        //添加到套餐菜品表
        setmealDishService.saveBatch(setmealDishes);
    }

    /**
     * 删除套餐
     * @param ids
     */
    @Override
    //开启事务，因为需要操作两张表
    @Transactional
    public void deleteWithDish(List<Long> ids) {
        LambdaQueryWrapper<Setmeal> setmealLambdaQueryWrapper = new LambdaQueryWrapper<>();
        setmealLambdaQueryWrapper.in(Setmeal::getId,ids);
        //查询套餐状态，是否在售
        setmealLambdaQueryWrapper.eq(Setmeal::getStatus,1);
        int count = this.count(setmealLambdaQueryWrapper);

        if (count>0){
            //如果不能删除，抛出业务异常
            throw new CustomException("该套餐在售，删除失败");
        }
        //如果可以删除，先删除套餐表中的数据 --setmeal
        //先删除关系表的数据的话，套餐表在售的话就数据缺失了
        this.removeByIds(ids);

        LambdaQueryWrapper<SetmealDish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.in(SetmealDish::getSetmealId,ids);
        //删除关系表中的数据
        setmealDishService.remove(dishLambdaQueryWrapper);
    }

    /**
     * 修改套餐回显
     * @param id
     * @return
     */
    @Override
    public SetmealDto getWithDishDto(Long id) {
        //通过id查询修改哪个套餐
        Setmeal setmeal = this.getById(id);
        //创建dto对象，里面添加了菜品字段
        SetmealDto setmealDto = new SetmealDto();
        //拷贝
        BeanUtils.copyProperties(setmeal, setmealDto);

        //创建查询条件  套餐中的菜品
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(id != null, SetmealDish::getSetmealId, id);
        List<SetmealDish> list = setmealDishService.list(queryWrapper);
//        封装
        setmealDto.setSetmealDishes(list);
        return setmealDto;
    }


}