package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.dto.DishDto;
import com.itheima.entity.Dish;
import com.itheima.entity.DishFlavor;
import com.itheima.mapper.DishMapper;
import com.itheima.service.DishFlavorService;
import com.itheima.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@SuppressWarnings("all")
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {
    @Autowired
    private DishFlavorService dishFlavorService;

    /**
     * 新增菜品
     * @param dishDto
     */
    @Override
    @Transactional
    public void saveWithFlavor(DishDto dishDto) {
        //先保存菜品dish基本信息
        this.save(dishDto);
        //获取菜品id
        Long id = dishDto.getId();
        //获取菜品口味集合，里面封装了前端提交过来的口味信息，但是没有绑定菜品id
        List<DishFlavor> flavors = dishDto.getFlavors();
        //循环遍历  将菜品id添加到口味表
        for (DishFlavor flavor : flavors) {
            flavor.setDishId(id);
        }
        //添加
        dishFlavorService.saveBatch(flavors);
    }

    /**
     * 数据回显
     * @param id
     * @return
     */
    @Override
    public DishDto getByIdWithFlavor(Long id) {
        //通过id查询
        Dish dish = this.getById(id);
        //创建dto对象  里面拓展了口味集合属性
        DishDto dishDto = new DishDto();
        //拷贝
        BeanUtils.copyProperties(dish,dishDto);

        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        //通过id查询口味表对应的菜品
        queryWrapper.eq(DishFlavor::getDishId,dish.getId());
        List<DishFlavor> list = dishFlavorService.list(queryWrapper);
        //封装到对象里面返回
        dishDto.setFlavors(list);
        return dishDto;
    }

    /**
     * 修改菜品
     * @param dishDto
     */
    @Override
    public void updateWithFlavor(DishDto dishDto) {
        //先更新菜品的基本信息
        this.updateById(dishDto);
        //通过id查询，先清理当前口味
        LambdaQueryWrapper<DishFlavor> dishFlavorLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishFlavorLambdaQueryWrapper.eq(DishFlavor::getDishId,dishDto.getId());
        dishFlavorService.remove(dishFlavorLambdaQueryWrapper);

        //添加提交过来的口味数据
        log.info(dishDto.getFlavors().toString());
        List<DishFlavor> flavors = dishDto.getFlavors();
        //循环遍历，设置每一个口味关联的菜品id
        for (DishFlavor flavor : flavors) {
            flavor.setDishId(dishDto.getId());
        }
        dishFlavorService.saveBatch(flavors);
    }
}