package com.itheima.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.entity.Orders;

import javax.servlet.http.HttpSession;

public interface OrdersService extends IService<Orders> {
    void submit(Orders orders, HttpSession session);
    Page getOrders(Integer page, Integer pageSize, String number, String beginTime, String endTime);
}
