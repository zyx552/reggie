package com.itheima.common;

/**
 * 保存当前登录用户的id
 */
@SuppressWarnings("ALL")
public class BaseContext {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    /**
     * 设置id值
     * @param id
     */
    public static void setThreadLocal(Long id) {
        threadLocal.set(id);
    }

    /**
     * 获取id值
     * @return
     */
    public static Long getCurrentId() {
        return threadLocal.get();
    }
}
