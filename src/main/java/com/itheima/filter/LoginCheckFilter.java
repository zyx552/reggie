package com.itheima.filter;

import com.alibaba.fastjson.JSON;
import com.itheima.common.BaseContext;
import com.itheima.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 检查用户是否登陆
 */
@Slf4j
@WebFilter
@SuppressWarnings("all")
public class LoginCheckFilter implements Filter {
    private static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        log.info("过滤器执行...");
        log.info("线程id为"+Thread.currentThread().getId());
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        //获取当前请求的URL
        String requestURI = request.getRequestURI();

        //定义不需要拦截的请求
        String[] uris = new String[]{
                "/employee/login",
                "/employee/logout",
                "/backend/**",
                "/front/**",
                "/common/**",
                "/user/sendMsg",
                "/user/login",
                "/doc.html",
                "/webjars/**",
                "/swagger-resources",
                "/v2/api-docs"
        };

        //判断当前请求是否需要处理
        boolean check = check(uris, requestURI);

        //不需要处理直接放行
        if (check) {
            filterChain.doFilter(request, response);
            return;
        }
        /**
         * 这个if(true)里面有一个Bug，用户端登录以后员工端可以不用登录就进入主界面，或者员工端登录以后用户端可以直接进入主界面
         */
        if(true){
            //判断是否已经登录(员工端)
            if (request.getSession().getAttribute("employee") != null) {
                Long employee = (Long)request.getSession().getAttribute("employee");
                //将id存入ThreadLocal
                BaseContext.setThreadLocal(employee);
                filterChain.doFilter(request, response);
                return;
            }

            //判断是否已经登录(用户端)
            if (request.getSession().getAttribute("user") != null) {
                Long userId = (Long)request.getSession().getAttribute("user");
                //将id存入ThreadLocal
                BaseContext.setThreadLocal(userId);
                filterChain.doFilter(request, response);
                return;
            }
        }


        //用户没有登录，给客户端响应数据
        response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));

    }

    /**
     * URL径匹配
     * @param uris
     * @param resUri
     * @return
     */
    public boolean check(String[] uris, String resUri) {

        //增强for循环，通过工具类判断URL是否相等
        for (String s : uris) {
            boolean match = PATH_MATCHER.match(s, resUri);
            if (match) {
                return true;
            }
        }
        return false;
    }
}
